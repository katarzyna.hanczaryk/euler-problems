package euler;

public class Problem14 {
    public static void main (String[] args) {
        //System.out.println(countElements(13));
        int max = 0;
        int temp = 0;
        int maxNum = 0;
        System.out.println(countElements(113383L));
        for (int i=10; i<1_000_000;i++) {
            System.out.println("checking " + i);
            temp=countElements(i);
            if (temp>max) {
                max=temp;
                maxNum = i;
            }
        }
        System.out.println("maxNum " + maxNum);
    }

    public static int countElements(long startingNum) {
        int counter = 1;
        while (startingNum!=1L) {
            if (startingNum%2==0) startingNum=startingNum/2;
            else startingNum = 3*startingNum+1;
            counter++;
        }

        return counter;
    }
}
