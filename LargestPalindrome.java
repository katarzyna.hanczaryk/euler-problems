package euler;

public class LargestPalindrome {
    public static void main (String args[]) {
        int result = 0;
        for (int i = 100; i<1000; i++) {
            for (int j=i; j<1000; j++) {
                int tempResult = i*j;
                if (isPalindrome(tempResult) && tempResult>result) {
                    result = tempResult;
                }
            }
        }
        System.out.println(result);
        //System.out.println(isPalindrome(140042));
        
    }

    public static boolean isPalindrome (int numToCheck) {
        String stringNum = String.valueOf(numToCheck);
        char[] digits = stringNum.toCharArray();
        int i = 0;
        int j = digits.length-1;
        while (i<=j) {
            if (digits[i]==digits[j]) {
                i++;
                j--;
            }
            else return false;
        }
        return true;
    }
}
