package euler;
import java.util.*;
import java.io.*;

public class Problem13 {
    public static void main (String args[]) {
        String fileName = "largeNum.txt";
        String line = null;
        int[][] digitLines = new int[100][50];
        String[] StringLines = new String[100];
        int i=0;
        try (FileReader fileReader = new FileReader(fileName);
                BufferedReader bufferedReader = new BufferedReader(fileReader))
        {
            
            while ((line=bufferedReader.readLine()) !=null) {
                StringLines[i] = line;
                i++;
            }
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
        
        for (int j=0; j<100;j++) { //ROWS
            for (int k=0; k<50;k++) { //COLUMNS
                digitLines[j][k] = StringLines[j].charAt(k)-48; 
            }
        }

        int[] sumArr = new int[50];
        for (int j=0; j<50;j++) {
            int sum = 0;
            for (int k=0; k<100; k++) {
                sum+=digitLines[k][j];
            }
            sumArr[j] = sum;
        }
        int[][] hundredFirstSums = new int[50][52];
        for (int k=0;k<50;k++) {
            int[] tempArr = create50digitNum(sumArr[k], k);
            hundredFirstSums[k] = create50digitNum(sumArr[k], k); 
        }

        int [] sumArr2 = new int[52];
        for (int k=0; k<52;k++) {
            int sum = 0;
            for (int l=0; l<50; l++) {
                sum+=hundredFirstSums[l][k];
            }
            sumArr2[k] = sum;
        }

        for (int el : sumArr2) System.out.println(el);
            System.out.println("============");
        
       /* for (int[] el : hundredFirstSums) {
            for (int el2 : el)
            System.out.println(el2);
        } */

        int[] result = new int[53];
        for (int k=sumArr2.length-1;k>=0;k--) {
            if (sumArr2[k]<10) result[k] = sumArr2[k];
            else {
                String tempEl = String.valueOf(sumArr2[k]);
                System.out.println(tempEl + "tempEl");
                result[k]=Integer.valueOf(tempEl.charAt(1))-48;
                System.out.println(result[k] + "result[k]" + " result[k+1] before" + sumArr2[k-1]);
                sumArr2[k-1] +=Integer.valueOf(tempEl.charAt(0))-48;
                System.out.println("after "  +  sumArr2[k-1]);
            }
        }
        for (int el : result) System.out.print(el);
    }

    public static int[] create50digitNum (int numToConvert, int startingIndex) {
        String stringNum = String.valueOf(numToConvert);
        int[] myDigitsArr = new int[52];
        int i=0, k=0;
        while (i<startingIndex) {
            myDigitsArr[i] = 0;
            i++;
        }
    while (k<3) {
        myDigitsArr[i] = stringNum.charAt(k)-48;
            i++;
            k++;
        }
        while (i<52) {
            myDigitsArr[i] = 0;
            i++;
        }
        return myDigitsArr;
    }
}




















