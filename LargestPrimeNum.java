package euler;

public class LargestPrimeNum {
    public static void main (String args[]) {
        int factor = 2;
        int savedFactor = 0;
        long divisionResult = 725L;
        while (divisionResult>=factor*factor) {
            if (divisionResult%factor==0) {
                savedFactor = factor;
                divisionResult=divisionResult/factor;
            }
            else factor++;
        }
        System.out.println("Savedfactor " + savedFactor + " divisionResult " + divisionResult);
    }
}
