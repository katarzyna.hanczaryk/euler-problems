package euler;

public class Traingular {
    
    public static void main (String args[]) {
        System.out.println(countDivisor(21));
        System.out.println(findDivisibleTrainagle(500));
    }
    
    public static long findDivisibleTrainagle (int maxNumDivisors) {
        long currentTraingle = 1L;
        int divisorCounter = 0;
        int tempToAdd = 2;
        while (divisorCounter<maxNumDivisors) {
            System.out.println("currentTraingle " + currentTraingle); 
            currentTraingle+=tempToAdd;
            tempToAdd++;
            divisorCounter = countDivisor(currentTraingle);
        }
        return currentTraingle;
    }

    public static int countDivisor(long numToCheck) {
        int divisorCounter = 2;
        int currentDivisor = 2;
        while (2*currentDivisor<=numToCheck) {
            if (numToCheck%currentDivisor==0) divisorCounter++;
            currentDivisor++;
        }
        return divisorCounter;
    }
}
