package euler;
import java.io.*;

public class Problem18 {
    public static void main (String args[]) {
        int [][] digitTriangle = new int[100][];
        try (BufferedReader br = new BufferedReader(new FileReader("p067_triangle.txt"))) {
            
            String line;
            int j=0;
            while ((line=br.readLine()) !=null) {
                String[] row = line.split(" ");
                int[] numRow = new int[row.length];
                for (int i=0; i<row.length;i++) {
                    int tempNum = Integer.valueOf(row[i]);
                    numRow[i] = tempNum;
                }
                digitTriangle[j++] = numRow;
                
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

       
        /* for (int[] el : digitTriangle) {
            for (int el2 : el) System.out.print(el2 + " ");
        }

        for (int i=0;i<14;i++) {//rows
            for (int j=0;j<digitTriangle[i].length;j++) { //columns;;
                digitTriangle[i+1][j] +=digitTriangle[i][j];
                System.out.println(j+ " j adding to: " + digitTriangle[i+1][j]);
                digitTriangle[i+1][j+1] +=digitTriangle[i][j];
                System.out.println(j+" j adding to: " + digitTriangle[i+1][j+1]);
            }
        }*/

        for (int i=0; i<100;i++) {
            for (int j=0; j<digitTriangle[i].length;j++) {
                digitTriangle[i][j] = calculateMax(digitTriangle, i, j);
                System.out.print(digitTriangle[i][j] + " ");
            }
            System.out.println(" ");
        }

        int result = 0;
        for (int el : digitTriangle[99]) {
            if (el>result) result = el;
            
            System.out.println("el " + el);
        }

        System.out.println("result " + result);
    }

    public static int calculateMax (int[][] triangle, int currentRow, int currentColumn) {
        int currentNum = triangle[currentRow][currentColumn];
        
        if (currentRow == 0) return currentNum;
        
        if (currentColumn==0) 
            return currentNum+triangle[currentRow-1][currentColumn];
        
        if (currentColumn==currentRow) 
            return currentNum+triangle[currentRow-1][currentColumn-1];
        
        {
            int neighbour1 = triangle[currentRow-1][currentColumn-1];
            int neighbour2 = triangle[currentRow-1][currentColumn];
            return currentNum+Math.max(neighbour1, neighbour2);
        }
    }
}
