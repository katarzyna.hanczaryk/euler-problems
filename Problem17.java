package euler;
import java.util.*;

public class Problem17 {
    public static void main (String args[]) {
        Map<Integer, String> allElements = new HashMap<Integer, String>();
        String[] allNames = { "one", "two", "three", "four", "five", "six", "seven", "eight", 
        "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety", "hundred", "thousand", "and" };
        for (int i=1; i<21; i++) {
            allElements.put(i, allNames[i-1]);
        }
        allElements.put(30, allNames[20]);
        allElements.put(40, allNames[21]);
        allElements.put(50, allNames[22]);
        allElements.put(60, allNames[23]);
        allElements.put(70, allNames[24]);
        allElements.put(80, allNames[25]);
        allElements.put(90, allNames[26]);
        allElements.put(100, allNames[27]);
        allElements.put(1000, allNames[28]);
        allElements.put(-1, allNames[29]);

        Set<Integer> mapSet = new HashSet<Integer>();
        mapSet = allElements.keySet();
        long result = 0L;
        for (int i=1; i<1001; i++) {
            result+=countLetters(i, allElements);
        }

        System.out.println("result " + result);
    }

    public static int countLetters (int numToCount, Map<Integer, String> elements) {
        String stringNum = String.valueOf(numToCount);
        char[] numArr = stringNum.toCharArray();
        int[] digitsArr = new int[4];
        for (int i=numArr.length-1, k=3; i>=0;i--, k--) {
            digitsArr[k] = Integer.valueOf(numArr[i])-48;
        }
        
        int letterSum = 0;
        if (digitsArr[2]==1) {
            if (digitsArr[3]==0) letterSum+=elements.get(10).length(); //11-19
            else if (digitsArr[3]==1) letterSum+=elements.get(11).length();
            else if (digitsArr[3]==2) letterSum+=elements.get(12).length();
            else if (digitsArr[3]==3) letterSum+=elements.get(13).length();
            else if (digitsArr[3]==4) letterSum+=elements.get(14).length();
            else if (digitsArr[3]==5) letterSum+=elements.get(15).length();
            else if (digitsArr[3]==6) letterSum+=elements.get(16).length();
            else if (digitsArr[3]==7) letterSum+=elements.get(17).length();
            else if (digitsArr[3]==8) letterSum+=elements.get(18).length();
            else if (digitsArr[3]==9) letterSum+=elements.get(19).length();
        } else {
            if (digitsArr[3]==1) letterSum+=elements.get(1).length(); //1-9
            else if (digitsArr[3]==2) letterSum+=elements.get(2).length();
            else if (digitsArr[3]==3) letterSum+=elements.get(3).length();
            else if (digitsArr[3]==4) letterSum+=elements.get(4).length();
            else if (digitsArr[3]==5) letterSum+=elements.get(5).length();
            else if (digitsArr[3]==6) letterSum+=elements.get(6).length();
            else if (digitsArr[3]==7) letterSum+=elements.get(7).length();
            else if (digitsArr[3]==8) letterSum+=elements.get(8).length();
            else if (digitsArr[3]==9) letterSum+=elements.get(9).length();

            if (digitsArr[2]==2) letterSum+=elements.get(20).length(); //20-90
            else if (digitsArr[2]==3) letterSum+=elements.get(30).length();
            else if (digitsArr[2]==4) letterSum+=elements.get(40).length();
            else if (digitsArr[2]==5) letterSum+=elements.get(50).length();
            else if (digitsArr[2]==6) letterSum+=elements.get(60).length();
            else if (digitsArr[2]==7) letterSum+=elements.get(70).length();
            else if (digitsArr[2]==8) letterSum+=elements.get(80).length();
            else if (digitsArr[2]==9) letterSum+=elements.get(90).length();
        }
        if (digitsArr[1]!=0) {
            letterSum+=elements.get(100).length();
            if (digitsArr[2]!=0 || digitsArr[3]!=0) letterSum+=elements.get(-1).length();
        }
        if (digitsArr[1]==1) letterSum+=elements.get(1).length(); //100-900
        else if (digitsArr[1]==2) letterSum+=elements.get(2).length();
        else if (digitsArr[1]==3) letterSum+=elements.get(3).length();
        else if (digitsArr[1]==4) letterSum+=elements.get(4).length();
        else if (digitsArr[1]==5) letterSum+=elements.get(5).length();
        else if (digitsArr[1]==6) letterSum+=elements.get(6).length();
        else if (digitsArr[1]==7) letterSum+=elements.get(7).length();
        else if (digitsArr[1]==8) letterSum+=elements.get(8).length();
        else if (digitsArr[1]==9) letterSum+=elements.get(9).length();
        if (digitsArr[0]!=0) letterSum+=elements.get(1000).length() + elements.get(1).length(); 
            return letterSum;
    }
}
