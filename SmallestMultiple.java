package euler;

public class SmallestMultiple {
    public static void main (String args[]) {
        //System.out.println(countMCM(128,140));
        System.out.println(countMCM(20));
    }

    public static int countMCD(int num1, int num2) {
        while (num1!=0 && num2!=0) {
            if (num1>=num2) num1=num1%num2;
            else num2=num2%num1;
        }
        if (num1!=0) return num1;
        else return num2;
    }

    public static int countMCM(int num1, int num2) {
        return (num1*num2)/countMCD(num1, num2);
    }

    public static int countMCM(int numOfEl) {
        int result = 0;
        if (numOfEl==2) return 2;
        result = countMCM(numOfEl, countMCM(numOfEl-1));
        System.out.println("result " + result);
        return result;
    }
}
