package euler;

public class SumSquareDifference {
    public static void main (String args[]) {
        int sumSquares = 0;
        int squareSum = 0;
        for (int i=1; i<=100; i++) {
            sumSquares += i*i;
            squareSum +=i;
        }
        System.out.println(sumSquares + " sumSquares " + squareSum + "squareSum");
        squareSum = squareSum*squareSum; 
        System.out.println(squareSum-sumSquares);
    }
}
