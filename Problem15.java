
public class Problem15 {
    public static void main (String args[]) {
        long[][] myArr = new long[21][21];
        myArr[0][0] = 1;
        for (int i=0; i<21; i++) { //rows
            for (int j=0; j<21;j++) { //columns
                if (i==0 && j==0) {
                    System.out.println("adding " + 1);
                    myArr[0][0] = 1;
                }
                else {
                    myArr[i][j] = nextEl(i, j, myArr);
                    System.out.println("adding " + nextEl(i,j,myArr));
                }
            }
        }
        for (int i=0; i<20; i++) {
            for (int j=0; j<20; j++) {
                System.out.print(myArr[i][j] + " ");
            } System.out.println("++++");
        }
        System.out.println(myArr[20][20]);
    }

    public static long nextEl (int row, int column, long[][] myArr) {
        long result = 0;
        if (column>0) {
            result = myArr[row][column-1];
        }
        if (row>0) {
            result += myArr[row-1][column];
        }
        return result;
    }

}
