package euler;
import java.util.*;

public class Problem16 {
    public static void main (String args[]) {
        List<Integer> test = new ArrayList<Integer>();
        test.add(5);
        test.add(1);
        test.add(2);
        List<Integer> twoPower1000 = new ArrayList<Integer>();
        twoPower1000.add(2);
        for (int i=2; i<1001; i++) {
            twoPower1000 = multiplyBy2(twoPower1000);
        }
        //List<Integer> testList = multiplyBy2(test);
        Integer finalResult = 0;
        for (Integer el : twoPower1000) finalResult+=el;
        System.out.println(finalResult);

    }

    public static Integer takeLastDigit (Integer numToCheck) {
        String stringNum = String.valueOf(numToCheck);
        char[] digitArr = stringNum.toCharArray();
        if (digitArr.length==1) return numToCheck;
        else return Integer.valueOf(digitArr[1])-48;
    }

    public static List<Integer> multiplyBy2 (List<Integer> digitsArr) {
        boolean elToAdd=false;
        for (int i=digitsArr.size()-1; i>=0; i--) {
            Integer tempDigit = digitsArr.get(i);
            Integer nextEl = takeLastDigit(tempDigit*2);

            digitsArr.set(i, takeLastDigit(tempDigit));
            if (elToAdd) {
                digitsArr.set(i, ++nextEl);
                elToAdd=false;
            }
            else digitsArr.set(i, nextEl);
            
            if (tempDigit>4) {
                 elToAdd=true;
            }
        }
        if (elToAdd) digitsArr.add(0,1);
        return digitsArr;
    }
}
