package euler;

public class EvenFibonacci {
    public static void main (String args[]) {
        int evenResult = 0;
        int nValue = 0;
        int nextValue = 1;
        while (nextValue<4000000) {
            int tempValue = nextValue;
            nextValue = nextValue + nValue;
            nValue = tempValue;
            
            if (nValue%2==0) 
            evenResult += nValue;
        }

        System.out.println(evenResult);
    }
}
