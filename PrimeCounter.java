package euler;
import java.util.*;

public class PrimeCounter {
    public List<Integer> primeList = new ArrayList<Integer>();
    public PrimeCounter() {
        primeList.add(2);
        primeList.add(3);
    }
    public static void main (String args[]) {
    }

    public boolean isPrime(int num) {
        int i = 0;    
        while (i<primeList.size() && primeList.get(i)*primeList.get(i)<=num) {
            if (num%primeList.get(i)==0) return false;
            else i++;
        }
        //looking for a next prime that needs to be added to the list
        if (i==primeList.size()) { 
            int newElement = primeList.get(i-1) + 1;

            while (newElement*newElement<=num) {
                if (isPrime(newElement)) {
                  System.out.println("adding: " + newElement + " size " + primeList.size());
                    primeList.add(newElement);
                    if (num%newElement==0) return false;
                    else newElement++;
                }    
                else newElement++;
            }
        }
            return true;
    }
    
}
